// Create Array
// use an array Literal - array literal
// const array1 = ['eat' , 'sleep'];
// console.log(array1);

const array2 = new Array('pray', 'play');
console.log(array2);


// empty array
const empArray = []; 

// Numeric Array

const numArray = [2, 3, 51, 20];



const newData1 = [

	{'task': 'exercise'},
	[1,2,3],
	function hello(){
		console.log("Hello I am array");
	}
];
console.log(newData1);


let arrayPlace = ['Japan' , 'Norway' , 'Korea' , 'Palawan' , 'Boracay', 'Canda' , 'Singapore'];

console.log(arrayPlace[0]);
console.log(arrayPlace);
console.log(arrayPlace[arrayPlace.length -1]);
console.log(arrayPlace.length);

for ( let i= 0; i < arrayPlace.length; i++){
	console.log(arrayPlace[i]);
}



// Array Manipulation
// Add element to an array - push();

let dailyActivities = ['eat' , 'work' , 'pray' , 'play'];
dailyActivities.push('exercise');//add element to the end
console.log(dailyActivities);

dailyActivities.unshift('sleep'); //add element to the beginning
console.log(dailyActivities);

dailyActivities[2] = 'sing';//replace element value to the index position 2
console.log(dailyActivities);


dailyActivities[6] = 'dance';
console.log(dailyActivities);


// Reassign the value or item to place
arrayPlace[3] = 'Giza Sphinx';
console.log(arrayPlace);

// Mini activity

arrayPlace[0] = 'Malinao, Aklan';
arrayPlace[arrayPlace.length-1] = 'Saint Joseph Academy';

console.log(arrayPlace);
console.log(arrayPlace[0]);
console.log(arrayPlace[arrayPlace.length-1]);


// Array Methonds
	// Manipulate array with pre determined Js Function
	// Mutators 

let array4=['Juan' , 'Pedro' ,'Jose' , 'Andres'];

array4[array4.length] = 'Francisco';
console.log(array4);


array4.pop();// remove last item on array
console.log(array4);

let removeItemShift = array4.shift();// remove 1st item on array
console.log(array4);
console.log(removeItemShift);







let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];


array1[array1.length] = 'Francisco';
	console.log(array1);

	// .push() - allows us to add an element at the end of the array
	array1.push('Andres');
	console.log(array1);

	// .unshift() - allows us to add an element at the beginning of the array
	array1.unshift('Simon');
	console.log(array1);

	// .pop() - allows us to delete or remove the last item/element at the end of the array
	array1.pop();
	console.log(array1);
	// .pop() is also able to return the item we removed.
	console.log(array1.pop());
	console.log(array1);

	let removedItem = array1.pop();
	console.log(array1);
	console.log(removedItem);

	// .shift() return the item we removed
	let removedItemShift = array1.shift();
	console.log(array1);
	console.log(removedItemShift);

// Mini Activity
array1.pop();
console.log(array1);
array1.shift();
console.log(array1);
array1.unshift('George');
console.log(array1);
array1.push('Michael');
console.log(array1);


// .sort() - by default it will allow us to sort our item in ascending order.

array1.sort();
console.log(array1);

let numArray1 = [3 , 2 , 1, 6, 7, 9];
numArray1.sort();
console.log(numArray1);


let numArray2 = [32, 400 , 450 , 2, 9, 50, 90];
numArray2.sort((a,b)=> a-b); //ascend or descend the number properly. a-b  = ascend b-a = descending
console.log(numArray2);
// let codeNameArray = ['123Melsmellow', 'Aaron245', 'Jose', '5673Warrior'];

// codeNameArray.sort();
// console.log(codeNameArray);


// Ascending sort per numbers value

numArray2.sort(function(a,b){
	return a-b;
})
console.log(numArray2);

// Descending sort
numArray2.sort(function(a,b){
	return b-a;
})
console.log(numArray2);

// .reverse Descending

let arrayString = ['Marie' , 'Zen', 'Elaine' , 'Jamie']

console.log(arrayString);


arrayString.sort();
console.log(arrayString);

arrayString.sort().reverse();
console.log(arrayString);

let beatles = ['George', 'John', 'Paul', 'Ringo'];

let lakerPlayers = ['Lebron',  'Davis', 'Westbrook' , 'Kobe', 'Shaq'];
// splice - allows to remove and add elements from a given index.
lakerPlayers.splice(0,0,'Caruso');
console.log(lakerPlayers);
lakerPlayers.splice(0,1);
console.log(lakerPlayers);
lakerPlayers.splice(0,3);
console.log(lakerPlayers);
lakerPlayers.splice(1,1);
console.log(lakerPlayers);
lakerPlayers.splice(1,0, 'Gasol', 'Fisher');
console.log(lakerPlayers);


// Non-mutators

// slice - allows us to get the portion of the original array and return a new array with the items selected.

let computerBrands = ['IBM' , 'HP', 'Apple','MSI'];

computerBrands.splice(2,2,'Compact','Toshiba' ,'Acer');
console.log(computerBrands);

let newBrands = computerBrands.slice(1,3);
console.log(computerBrands);
console.log(newBrands);

let fonts = ['Time new Roman' ,'Comic Sans MS' , 'Impact', 'Monotype Corciva', 'Arial', 'Arial Black'];

// console.log(fonts);
// let newFontSet = fonts.slice(1,4);
// console.log(newFontSet);

console.log(fonts);
let newFonts = fonts.slice(1,4);
console.log(newFonts);
let newFonts1 = fonts.slice(2);
console.log(newFonts1);


// Mini Activity

let videoGame = ['PS4' , 'PS5', 'Switch' ,'Xbox' , 'Xbox1']

let microsoft = videoGame.slice(3);

let nintendo = videoGame.slice(2,4);

console.log(microsoft);
console.log(nintendo);

// toString 

let sentence = ['I', 'like','JavaScript', '.' , 'It' , 'is' , 'fun'];

let sentenceString = sentence.toString();
console.log(sentence);
console.log(sentenceString);

// .join

let sentence2 = ['My' ,'favorite','fastfood','is','Army Navy'];

let sentenceJoin = sentence2.join('');
console.log(sentenceJoin);


// Mini Activity

let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

let slice1 = charArr.slice(5,11);
let name1 = slice1.join('');
let slice2 = charArr.slice(13,19);
let name2 = slice2.join('');

console.log(name1);
console.log(name2);

// .concat()

let taskFriday = ['Drink HTML' , 'Eat Java'];
let taskSaturday = ['Inhale CSS', 'Breath Bootstrap'];
let taskSunday = ['Get Git', 'Be node'];

let weekendTasks = taskFriday.concat(taskSaturday,taskSunday);

console.log(weekendTasks);

// Accesors


let batch131 = [

	'Paolo',
	'Jamir',
	'Jed',
	'Ronel',
	'Jayson'
];

// indexOf
console.log(batch131.indexOf('Jed'));
console.log(batch131.indexOf('Ronel'));
console.log(batch131.indexOf('Ronel'));

// lastIndexOf

console.log(batch131.lastIndexOf('Jamir'));
console.log(batch131.lastIndexOf('Jayson'));

/*
	Mini-Activity
	Given a set of brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input the last time it was found in the array.
*/

		let carBrands = [
			'BMW',
			'Dodge',
			'Maserati',
			'Porsche',
			'Chevrolet',
			'Ferrari',
			'GMC',
			'Porsche',
			'Mitsubhisi',
			'Toyota',
			'Volkswagen',
			'BMW'
		];

function carIndex(car, car2){
	console.log(carBrands.indexOf(car));
	console.log(carBrands.lastIndexOf(car2));
}

carIndex('BMW', 'Toyota');

// Iterator Methods

let avengers = [
	
	'Hulk',
	'Black Widow',
	'Hawkeye',
	'Spiderman',
	'Iron Man',
	'Captain America'

];


avengers.forEach(function(avenger){
	console.log(avenger);
})

let marvelHeroes = [
	'Moon Knight',
	'Jessica Jones',
	'Deadpool',
	'Cyclops'
];

marvelHeroes.forEach(function(hero){
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		avengers.push(hero);
	}
});
console.log(avengers);


// map()

let number = [25, 50, 30 ,10 , 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number *5 ;
})

console.log(mappedNumbers);


// every()


let allMemberAge = [25, 30 , 15, 20 ,26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18
});

console.log(checkAllAdult);

// some()

let examScores = [75, 80 , 74, 71];

let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
});

console.log(checkForPassing);



// filter 

let numberArr2 = [500, 12 , 120 , 60, 6, 30];

let divisibleBy5 = numberArr2.filter(function(number){
	return number % 5 === 0 ;

});

console.log(divisibleBy5);


// find()


let registeredUsernames = ['pedro101' , 'mikeyTheKing2000' ,'superPhoenix' , 'sheWhoCode'];


let foundUser = registeredUsernames.find(function(username){
	console.log(username);
	return username === 'mikeyTheKing2000';
});
console.log(foundUser);


// include

let registeredEmails = [
		'JohnnyPhoenix1991@gmail.com',
		'michaelKing@gmail.com',
		'pedro_himself@yahoo.com',
		'sheJonesSmith@gmail.com'
];

let doesEmailExist = registeredEmails.includes('michaelKing@gmail.com');

console.log(doesEmailExist);



// Mini Activity
/*
	Mini-Activity
	Create 2 functions 
		First Function is able to find specified or the username input in our registeredUsernames array.
		Display the result in the console.

		Second Function is able to find a specified email already exist in the registeredEmails array.
			- IF  there is an email found, show an alert:
				"Email Already Exist."
			- IF there is no email found, show an alert:
				"Email is available, proceed to registration."

		You may use any of the three methods we discussed recently.

*/

let findUsername = registeredUsernames.find(function(username){
	console.log(username);
	return username === 'superPhoenix';
});
console.log(findUsername);


function checkEmail(email){

     let doesEmailExist2 = registeredEmails.includes(email);
     if(doesEmailExist2 === true ){
     	alert('Email Exist');
     }else{
     	alert('Proceed to Registration');
     }
}
	
checkEmail('michaelKing@gmail.com')
console.log(doesEmailExist2);
